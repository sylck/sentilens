#! /bin/bash

dotenv=src/.env
if [[ ! -f "$dotenv" ]]; then
  echo "'.env' file not found. Please create a '.env' file in 'src' before running."
  exit 1
fi

file=bin/activate
if [[ -f "$file" ]]; then
    echo "'$file' exists. Setup already performed. Aborting."
    exit 1
fi

# load dotenv file
export $(tr -ds "' " "\n" < src/.env)

activate=bin/activate
if [[ ! -f "$activate" ]]; then
    echo "Creating virtual environment..."
    virtualenv -p python3 .
fi

source $activate
echo "Installing dependencies..."
pip install -r requirements.txt

echo "Creating PostgreSQL database and user..."
sudo -u postgres psql -c "create extension postgis"
sudo -u postgres createdb $DB_DATABASE
sudo -u postgres createuser -s $DB_USERNAME
sudo -u postgres psql -c "alter user $DB_USERNAME with encrypted password '$DB_PASSWORD'";
sudo -u postgres psql -c "grant all privileges on database $DB_DATABASE to $DB_USERNAME";

echo "Running migrations..."
python src/manage.py migrate

echo "Creating administrator user account..."
python src/manage.py createsuperuser

chmod +x run.sh

echo "Setup completed."