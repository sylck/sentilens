window.onload = main;

function main() {
    var ui = new Ui();
    ui.setup();
}

function isDefined(arg) {
    return typeof(arg) !== "undefined";
}

function hideElementJquery(el) {
    el.addClass('hidden');
}

function showElementJquery(el) {
    el.removeClass('hidden');
}

function Ui() {
    /****************
     *    init()    *
     ****************/
    // DOM elements
    var body = $('body');

    var map = new Map();


    /*****************
     *    methods    *
     *****************/
    this.setup = function () {
        map.setup(ready);
    };


    /*******************
     *    functions    *
     *******************/
    function ready() {
        hideLoader();
    }

    function hideLoader() {
        body.removeClass('loading');
    }
}

function Map() {
    /****************
     *    init()    *
     ****************/
    const MAX_ZOOM_LEVEL = 13;
    const UNDEFINED_RASTERLAYER_ID = -1;

    /*
     * starting position. used if there are no tilesets or if it fits
     * comfortably within active tileset's bounds at `MAX_ZOOM_LEVEL`.
     * should not be a value in the middle of a water body, like (0, 0)
     * (latitude, longitude), as there is no overlay data to salvage
     * empty screen when no tilesets are available.
     */
    const START_LAT = 51,
          START_LON = 0;

    var containerId = 'map';

    var api = new Api();
    var yearSlider = new MapYearControl(yearChangeHandler);

    var sentinelLayer;
    var omsHybridLayer;

    var readyCallback;
    var lmap;
    var tileSets;

    init();


    /*****************
     *    methods    *
     *****************/
    this.setup = function (_readyCallback) {
        readyCallback = _readyCallback;
        api.fetchTileSets(tileSetsFetched);
    };


    /*******************
     *    functions    *
     *******************/
    function init() {
        sentinelLayer = L.tileLayer(
            'tiles/{rasterlayer_id}/{z}/{x}/{y}.png',
            {
                attribution: 'Imagery derived from tiles captured by Sentinel-2 satellites of Copernicus programme',
                rasterlayer_id: UNDEFINED_RASTERLAYER_ID
            }
        );
        omsHybridLayer = L.tileLayer(
            'https://maps.heigit.org/openmapsurfer/tiles/hybrid/webmercator/{z}/{x}/{y}.png',
            {
                attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> | Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }
        );

        lmap = L.map(
            containerId,
            {
                maxZoom: MAX_ZOOM_LEVEL,

                /*
                 * no bounce when constraining panning and zooming to
                 * tileset (tilelayer) bounds
                 */
                maxBoundsViscosity: 1,
                layers: [sentinelLayer, omsHybridLayer],
                center: new L.LatLng(START_LAT, START_LON),
                zoom: MAX_ZOOM_LEVEL
            }
        );

        L.control.scale({position: 'bottomright'}).addTo(lmap);
    }

    function tileSetsFetched(success, _tileSets) {
        tileSets = _tileSets;

        finalizeSetup();
    }

    function finalizeSetup() {
        yearSlider.setItems(tileSets);
        yearSlider.getLcontrol().addTo(lmap);

        readyCallback();
    }

    function setActiveTileSet(tileSet) {
        // array of bounds plain objects
        var boundPObjs = tileSet.bounds;

        if (boundPObjs.length === 2) {
            var bounds = L.latLngBounds(
                new L.LatLng(
                    boundPObjs[0].latitude,
                    boundPObjs[0].longitude
                ),
                new L.LatLng(
                    boundPObjs[1].latitude,
                    boundPObjs[1].longitude
                )
            );

            lmap.setMaxBounds(bounds);

            var minZoom = lmap.getBoundsZoom(bounds, true);
            lmap.setMinZoom(minZoom);
        }
        else {
            /*
             * probably an empty array, meaning that the tileset is not
             * finalized, bounds not computed and defined yet, so unset
             * previously defined values
             */
            lmap.setMaxBounds(undefined);
            lmap.setMinZoom(undefined);
        }

        sentinelLayer.options.rasterlayer_id = tileSet.rasterlayer_id;
        sentinelLayer.redraw();
    }

    function yearChangeHandler(year) {
        var tileSet;

        for (var i = 0; i < tileSets.length; i++) {
            var ts = tileSets[i];

            if (ts.year === year) {
                tileSet = ts;
                break;
            }
        }

        // don't call setActiveTileSet if tileSet not found
        if (!!tileSet){
            setActiveTileSet(tileSet);
        }
    }
}

function MapYearControl(_yearChangeHandler) {
    /****************
     *    init()    *
     ****************/
    const SENTINEL2_YEAR_OP_START = 2015;

    var yearChangeHandler = _yearChangeHandler;

    var lcontrol;
    var template = $('#myc-tmpl').html();

    var contentVisible;
    var togglable;

    // DOM elements
    var containerDom;
    var container;
    var toggle;
    var content;

    init();


    /*****************
     *    methods    *
     *****************/
    this.getLcontrol = function () {
        return lcontrol;
    };

    this.setItems = function (items) {
        setSliderRangeFromTileSets(items);
    };


    /*******************
     *    functions    *
     *******************/
    function init() {
        lcontrol = L.control({position: 'topleft'});

        containerDom = L.DomUtil.create('div', 'year-control');
        L.DomEvent.disableClickPropagation(containerDom);
        containerDom.innerHTML = template;

        container = $(containerDom);

        toggle = container.find('.toggle');
        toggle.a = toggle.find('a');
        toggle.on('click', togglePressed);
        disableToggle();

        content = container.find('.content');
        hideContent();

        content.closeButton = content.find('.close');
        content.closeButton.on('click', hideContent);

        content.yearLabel = content.find('.year');

        content.slider = content.find('.slider');
        content.sliderDom = content.slider[0];
        noUiSlider.create(content.sliderDom, {
            range: {'min': [0], 'max': [100]},
            start: 0,
            snap: true
        });
        content.sliderDom.noUiSlider.on('slide', yearChanged);
        disableSlider();

        lcontrol.onAdd = getContainer;
    }

    function getContainer() {
        return containerDom;
    }

    function hideContent() {
        hideElementJquery(content);
        contentVisible = false;
    }

    function showContent() {
        showElementJquery(content);
        contentVisible = true;
    }

    function togglePressed(event) {
        // prevent adding event sender href value to URL
        event.preventDefault();

        if (!togglable) {
            return;
        }

        if (contentVisible) {
            hideContent();
        }
        else {
            showContent();
        }
    }

    function yearChanged(values, handle) {
        var yearStr = values[handle];
        var year = parseInt(yearStr);

        setYearLabelValue(year);
        yearChangeHandler(year);
    }

    function setYearLabelValue(val) {
        content.yearLabel.text(val)
    }

    function disableSlider() {
        content.sliderDom.setAttribute('disabled', true);
    }

    function enableSlider() {
        content.sliderDom.removeAttribute('disabled');
    }

    function disableToggle() {
        toggle.a.addClass('leaflet-disabled');
        togglable = false;
    }

    function enableToggle() {
        toggle.a.removeClass('leaflet-disabled');
        togglable = true;
    }

    function setSliderRange(range) {
        content.sliderDom.noUiSlider.updateOptions({
            range: range
        });
    }

    function setSliderValue(value) {
        content.sliderDom.noUiSlider.set(value);
    }

    /**
     * Convert floating-point number to string. Return simple string representation of number if does not have decimal places, else round to one decimal place and return as a string.
     *
     * @param {number} val - A floating-point number to be converted to string
     * @return {string}
     */
    function floatToStr(val) {
        if (val % 1 === 0) {
            // does not have decimal places
            return val.toString();
        }
        else {
            // has decimal places
            return val.toFixed(1);
        }
    }

    function setSliderRangeFromTileSets(tileSets) {
        var shouldDisableToggle = false;
        var shouldDisableSlider = false;
        var shouldCallYch = true;
        var i;

        /*
         * step 1: convert tileset list to year list
         */
        var years = [];

        for (i = 0; i < tileSets.length; i++) {
            years.push(tileSets[i].year);
        }

        if (years.length === 0) {
            years.push(SENTINEL2_YEAR_OP_START);
            years.push(SENTINEL2_YEAR_OP_START + 1);

            shouldDisableSlider = true;

            /*
             * no tilesets, so no point in showing read-only content as
             * data shown is not accurate and thus useful
             */
            shouldDisableToggle = true;

            /*
             * don't call yearChangeHandler, there is no tileSet to set
             * as active
             */
            shouldCallYch = false;
        }
        else if (years.length === 1) {
            years.push(years[0] + 1);

            /*
             * disable slider, but keep content visibility togglable so
             * that user can check which year does shown data belong to
             */
            shouldDisableSlider = true;
        }

        /*
         * step 2: convert year list to range
         */
        var range = {};

        var minYear = years[0];
        var maxYear = years[years.length - 1];

        range.min = minYear;
        range.max = maxYear;

        // add years that might be in between to range
        var maxYearDelta = maxYear - minYear;

        for (i = 1; i < years.length - 1; i++) {
            var year = years[i];
            var yearDelta = year - minYear;
            var yearPerc = (yearDelta / maxYearDelta) * 100;
            var yearPercStr = floatToStr(yearPerc) + '%';
            range[yearPercStr] = year;
        }

        /*
         * step 3: update slider range and set value to range.min
         */
        setSliderRange(range);
        setSliderValue(range.min);
        setYearLabelValue(range.min);

        // disable slider if there is nothing to slide to
        if (shouldDisableSlider) {
            disableSlider();
        }
        else {
            enableSlider();
        }

        if (shouldDisableToggle) {
            disableToggle();
        }
        else {
            enableToggle();
        }

        if (shouldCallYch) {
            yearChangeHandler(range.min);
        }
    }
}

function Api() {
    /****************
     *    init()    *
     ****************/
    const ENDPOINT_TILESETS_INDEX = '/api/tilesets/';


    /*****************
     *    methods    *
     *****************/
    this.fetchTileSets = function (callback) {
        $.ajax(ENDPOINT_TILESETS_INDEX, {
            method: "GET",
            success: function (d) { xhrCompleted(callback, d); },
            error: function (d) { xhrFailed(callback, d); }
        });
    };


    /*******************
     *    functions    *
     *******************/
    function xhrCompleted(callback, response) {
        callback(true, response);
    }

    function xhrFailed(callback, response) {
        var r = response.responseJSON;
        callback(false, r);
    }
}