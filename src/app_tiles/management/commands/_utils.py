import os
import tempfile
import zipfile
import cvxpy
import geojson
import math
import numpy
import psutil
import rasterio

from functools import partial
from multiprocessing import Pool
from maxrect import pts_to_leq
from rasterio.merge import merge
from requests import HTTPError
from shapely.geometry import shape, Polygon

from django.db.models import Q
from django import db
from django.contrib.gis.gdal import GDALRaster, OGRGeometry, Envelope

from raster.models import RasterTile
from raster.tiles.const import (
    WEB_MERCATOR_TILESIZE, INTERMEDIATE_RASTER_FORMAT, WEB_MERCATOR_SRID
)
from raster.tiles.utils import tile_bounds, tile_scale, tile_index_range

from s2_tci.download import download_file
from s2_tci.find import get_tci_download_url

from ..commands import _const as const


#
# utility functions related to tiles
#
def _get_tileids_for_area_worker(tile, area_shape):
    tile_geometry = tile['geometry']
    tile_shape = shape(tile_geometry)

    if tile_shape.intersects(area_shape):
        tileid = tile['properties']['name']
        return tileid


def get_tileids_for_area(world_tiles, area_gj):
    area_dict = dict(area_gj)
    area_geometry = area_dict['features'][0]['geometry']
    area_shape = shape(area_geometry)

    worker = partial(
        _get_tileids_for_area_worker,
        area_shape=area_shape
    )

    with Pool() as pool:
        tileids = pool.map(worker, world_tiles)

    tileids = [tileid for tileid in tileids if tileid is not None]
    return tileids


def make_mosaic_from_paths(paths, dest):
    rio_rasters = [rasterio.open(path) for path in paths]
    mosaic, tf_info = merge(rio_rasters)

    meta = rio_rasters[0].meta.copy()
    meta.update({
        'width': mosaic.shape[2],
        'height': mosaic.shape[1],
        'transform': tf_info
    })

    with rasterio.open(dest, 'w', **meta) as outfile:
        outfile.write(mosaic)


def make_tslice(zoom_level, x, y, raster, tslice_path):
    tilesize = WEB_MERCATOR_TILESIZE
    bounds = tile_bounds(x, y, zoom_level)
    tilescale = tile_scale(zoom_level)

    tslice = raster.warp(
        {
            'name': tslice_path,
            'origin': [bounds[0], bounds[3]],
            'scale': [tilescale, -tilescale],
            'width': tilesize,
            'height': tilesize
        },
        const.GDAL_RESAMPLE_ALGORITHM
    )

    return tslice


def _raster_to_bands(raster):
    return [{
        'data': band.data(),
        'nodata_value': band.nodata_value
    } for band in raster.bands]


def has_nodata(raster):
    bands = _raster_to_bands(raster)
    return any([numpy.any(band['data'] == band['nodata_value']) for band in bands])


def _has_data(raster):
    bands = _raster_to_bands(raster)
    return any([numpy.any(band['data'] != band['nodata_value']) for band in bands])


def tile_already_generated(rasterlayer, zoom_level, x, y):
    # even if not manually defined in model class, model manager is
    # added to model on the fly, so inspection warnings for `objects`
    # can be safely ignored.

    # noinspection PyUnresolvedReferences
    return RasterTile.objects.filter(
        rasterlayer=rasterlayer,
        tilex=x,
        tiley=y,
        tilez=zoom_level
    ).exists()


def make_temp_tif(tmp_dir):
    return tempfile.NamedTemporaryFile(
        suffix=f'.{INTERMEDIATE_RASTER_FORMAT}',
        dir=tmp_dir
    )


def add_tile_to_db(tile_raster, rasterlayer, zoom_level, x, y):
    # noinspection PyUnresolvedReferences
    RasterTile.objects.create(
        rast=tile_raster,
        rasterlayer=rasterlayer,
        tilex=x,
        tiley=y,
        tilez=zoom_level
    )


def _try_reuse_raster_for_x(x, raster, tmp_dir, rasterlayer, ymin, ymax):
    zoom_level = const.MAX_ZOOM_LEVEL

    for y in range(ymin, ymax + 1):
        if tile_already_generated(rasterlayer, zoom_level, x, y):
            continue

        tstmpfile = make_temp_tif(tmp_dir)
        tslice = make_tslice(zoom_level, x, y, raster, tstmpfile.name)

        # don't reuse partial tiles, dedicated runs might fill slice
        # better
        if has_nodata(tslice):
            continue

        add_tile_to_db(tslice, rasterlayer, zoom_level, x, y)


def _try_reuse_raster_worker(x, raster_path, tmp_dir, rasterlayer, ymin, ymax):
    _check_free_ram_or_memerr()

    raster = GDALRaster(raster_path)

    _try_reuse_raster_for_x(x, raster, tmp_dir, rasterlayer, ymin, ymax)


def try_reuse_raster_multiproc(raster, tmp_dir, rasterlayer, xmin, ymin, xmax, ymax):
    worker = partial(
        _try_reuse_raster_worker,

        raster_path=raster.name, tmp_dir=tmp_dir, rasterlayer=rasterlayer,
        ymin=ymin, ymax=ymax
    )

    with _DBPool() as pool:
        pool.map(worker, range(xmin, xmax + 1))


def try_reuse_raster_singleproc(raster, tmp_dir, rasterlayer, xmin, ymin, xmax, ymax):
    for x in range(xmin, xmax + 1):
        _try_reuse_raster_for_x(x, raster, tmp_dir, rasterlayer, ymin, ymax)


def _build_tile_pyramid_for_x(x, tmp_dir, rasterlayer, zoom_level, ymin, ymax):
    for y in range(ymin, ymax):
        if tile_already_generated(rasterlayer, zoom_level, x, y):
            continue

        # take next smaller zoom level tiles for mosaicing
        xmin_ns = 2 * x
        ymin_ns = 2 * y
        xmax_ns = 2 * x + 1
        ymax_ns = 2 * y + 1

        query = (Q(tilex__gte=xmin_ns) & Q(tilex__lte=xmax_ns)) & \
                (Q(tiley__gte=ymin_ns) & Q(tiley__lte=ymax_ns)) & \
                Q(tilez=zoom_level + 1) & Q(rasterlayer=rasterlayer)

        # noinspection PyUnresolvedReferences
        tiles = RasterTile.objects.filter(query)

        if not tiles.exists():
            continue

        ttmpfiles = []
        for tile in tiles:
            # ignore empty tiles
            if not _has_data(tile.rast):
                continue

            ttmpfile = make_temp_tif(tmp_dir)
            tile.rast.warp({
                'name': ttmpfile.name,
                'driver': INTERMEDIATE_RASTER_FORMAT
            })
            ttmpfiles.append(ttmpfile)

        # none of retrieved tiles had data, don't bother mosaicing and
        # adding to the database
        if len(ttmpfiles) == 0:
            continue

        # merge all retrieved tiles
        mtmpfile = make_temp_tif(tmp_dir)
        make_mosaic_from_paths(
            [tf.name for tf in ttmpfiles],
            mtmpfile.name
        )
        mosaic = GDALRaster(mtmpfile.name)

        tstmpfile = make_temp_tif(tmp_dir)
        tslice = make_tslice(zoom_level, x, y, mosaic, tstmpfile.name)

        # don't add to database if at least one pixel is NOT nodata
        if not _has_data(tslice):
            continue

        add_tile_to_db(tslice, rasterlayer, zoom_level, x, y)


def _build_tile_pyramid_worker(x, tmp_dir, rasterlayer, zoom_level, ymin, ymax):
    _check_free_ram_or_memerr()
    _build_tile_pyramid_for_x(x, tmp_dir, rasterlayer, zoom_level, ymin, ymax)


def build_tile_pyramid_multiproc(tmp_dir, rasterlayer):
    for zoom_level in _reversed_range(const.MAX_ZOOM_LEVEL - 1):
        xmin, ymin, xmax, ymax = tile_index_range(
            _world_bbox_webmercator(), zoom_level
        )

        worker = partial(
            _build_tile_pyramid_worker,

            tmp_dir=tmp_dir, rasterlayer=rasterlayer, zoom_level=zoom_level,
            ymin=ymin, ymax=ymax
        )

        with _DBPool() as pool:
            pool.map(worker, range(xmin, xmax))


def build_tile_pyramid_singleproc(tmp_dir, rasterlayer):
    for zoom_level in _reversed_range(const.MAX_ZOOM_LEVEL - 1):
        xmin, ymin, xmax, ymax = tile_index_range(
            _world_bbox_webmercator(), zoom_level
        )

        for x in range(xmin, xmax):
            _build_tile_pyramid_for_x(
                x, tmp_dir, rasterlayer, zoom_level, ymin, ymax
            )


#
# utility functions related to products
#
def get_product(api, year, tileid):
    dateformat = '{year:04}{month:02}{day:02}'
    start = dateformat.format(year=year, month=1, day=1)
    end = dateformat.format(year=year, month=12, day=31)

    products = api.query(
        date=(start, end),
        platformname='Sentinel-2',
        order_by='cloudcoverpercentage',
        filename=f'*{tileid}*',
        limit=const.API_LIMIT_MAXRESULTSNO
    )

    if len(products) == 0:
        return None

    product_uuid, product = _first_in_collection(products.items())
    return product


def product_exists(api, tileid):
    products = api.query(
        platformname='Sentinel-2',
        filename=f'*{tileid}*',
        limit=const.API_LIMIT_MAXRESULTSNO
    )

    return len(products) != 0


def download_tci(api, tci_dir, safe_dir, product):
    try:
        url = get_tci_download_url(
            product['link_alternative'], product['title'], api.session
        )
        path = download_file(url, tci_dir, api.session)
    except HTTPError:
        # Sentinel servers acting up, get the whole SAFE package
        product_info = api.download(product['uuid'], safe_dir)

        # find TCI in it (seek for largest in case of working with L2A
        # tile) and extract to tci_dir
        safe_path = product_info['path']
        with zipfile.ZipFile(safe_path, 'r') as safe_zip:
            safe_files = safe_zip.infolist()
            tci_files = [
                file for file in safe_files if 'TCI' in file.filename
            ]

            if len(tci_files) > 1:
                target_tci = _find_largest_in_zil(tci_files)
            else:
                target_tci = tci_files[0]

            target_tci.filename = os.path.basename(target_tci.orig_filename)
            path = safe_zip.extract(target_tci, tci_dir)

    return path


#
# coordinates and transformations
#
def tile_to_lon(zoom_level, x):
    n = 2.0 ** zoom_level

    lon_deg = x / n * 360.0 - 180.0

    return lon_deg


def tile_to_lat(zoom_level, y):
    n = 2.0 ** zoom_level

    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * y / n)))
    lat_deg = math.degrees(lat_rad)

    return lat_deg


def _world_minlon():
    return tile_to_lon(0, 0)


def _world_maxlon():
    return -_world_minlon()


def _world_minlat():
    return tile_to_lat(0, 0)


def _world_maxlat():
    return -_world_minlat()


def _bbox_from_wgs84_to_webmercator(bbox_wgs84):
    # based on code from method `extent`, class `RasterLayer`, package
    # raster.models
    geom = OGRGeometry(Envelope(bbox_wgs84).wkt)
    geom.srid = const.WGS84_SRID
    geom.transform(WEB_MERCATOR_SRID)

    coords = geom.coords[0]
    xvals = [c[0] for c in coords]
    yvals = [c[1] for c in coords]

    bbox = (min(xvals), min(yvals), max(xvals), max(yvals))

    return bbox


def _world_bbox_webmercator():
    world_bbox_wgs84 = (
        _world_minlon(),
        _world_maxlat(),
        _world_maxlon(),
        _world_minlat()
    )

    return _bbox_from_wgs84_to_webmercator(world_bbox_wgs84)


def _tile_to_bbox(zoom_level, x, y):
    return {
        'top': tile_to_lat(zoom_level, y),
        'right': tile_to_lon(zoom_level, x + 1),
        'bottom': tile_to_lat(zoom_level, y + 1),
        'left': tile_to_lon(zoom_level, x)
    }


def _bbox_to_polygon_points(bbox):
    return [
        (bbox['left'], bbox['top']),
        (bbox['right'], bbox['top']),
        (bbox['right'], bbox['bottom']),
        (bbox['left'], bbox['bottom']),

        # same starting and ending point - a GIS thing. start and end
        # point have to be the same so that the described polygon is
        # properly closed
        # (https://gis.stackexchange.com/q/10308)
        (bbox['left'], bbox['top'])
    ]


def tile_to_polygon_points(zoom_level, x, y):
    return _bbox_to_polygon_points(_tile_to_bbox(zoom_level, x, y))


def tile_to_geojson(zoom_level, x, y):
    points = tile_to_polygon_points(zoom_level, x, y)

    # ignore PyCharm argument type warning for geojson.Polygon() -
    # `points` array needs to be wrapped in brackets, otherwise resultant
    # Polygon's `is_valid` attribute gets set to False

    # noinspection PyTypeChecker
    polygon = geojson.Polygon([points])
    feature = geojson.Feature(geometry=polygon)
    gj = geojson.FeatureCollection([feature])

    return gj


def get_maximal_rectangle(coordinates):
    """Find the largest, inscribed, axis-aligned rectangle.

    Largely based on function `get_maximal_rectangle` from package
    `maxrect`.

    :param coordinates:
        A list of of [x, y] pairs describing a closed, convex polygon.
    """

    coordinates = numpy.array(coordinates)
    x_range = (numpy.max(coordinates, axis=0)[0]
               - numpy.min(coordinates, axis=0)[0])
    y_range = (numpy.max(coordinates, axis=0)[1]
               - numpy.min(coordinates, axis=0)[1])

    scale = numpy.array([x_range, y_range])
    sc_coordinates = coordinates / scale

    poly = Polygon(sc_coordinates)
    inside_pt = (poly.representative_point().x,
                 poly.representative_point().y)

    A1, A2, B = pts_to_leq(sc_coordinates)

    bl = cvxpy.Variable(2)
    tr = cvxpy.Variable(2)
    br = cvxpy.Variable(2)
    tl = cvxpy.Variable(2)
    obj = cvxpy.Maximize(cvxpy.log(tr[0] - bl[0]) + cvxpy.log(tr[1] - bl[1]))
    constraints = [bl[0] == tl[0],
                   br[0] == tr[0],
                   tl[1] == tr[1],
                   bl[1] == br[1],
                   ]

    for i in range(len(B)):
        if inside_pt[0] * A1[i] + inside_pt[1] * A2[i] <= B[i]:
            constraints.append(bl[0] * A1[i] + bl[1] * A2[i] <= B[i])
            constraints.append(tr[0] * A1[i] + tr[1] * A2[i] <= B[i])
            constraints.append(br[0] * A1[i] + br[1] * A2[i] <= B[i])
            constraints.append(tl[0] * A1[i] + tl[1] * A2[i] <= B[i])

        else:
            constraints.append(bl[0] * A1[i] + bl[1] * A2[i] >= B[i])
            constraints.append(tr[0] * A1[i] + tr[1] * A2[i] >= B[i])
            constraints.append(br[0] * A1[i] + br[1] * A2[i] >= B[i])
            constraints.append(tl[0] * A1[i] + tl[1] * A2[i] >= B[i])

    prob = cvxpy.Problem(obj, constraints)
    prob.solve(solver=cvxpy.SCS, verbose=False, max_iters=100000,
               use_indirect=False)

    bottom_left = numpy.array(bl.value).T * scale
    top_right = numpy.array(tr.value).T * scale

    return list(bottom_left), list(top_right)


#
# other utility functions
#
def _reversed_range(start_i):
    return range(start_i, -1, -1)


def _DBPool():
    db.connections.close_all()
    return Pool()


def _check_free_ram_or_memerr():
    free_ram_b = psutil.virtual_memory().free

    if free_ram_b < const.MIN_FREE_RAM_BYTES:
        raise MemoryError


def _find_largest_in_zil(zipinfos):
    largest = None

    for zipinfo in zipinfos:
        if largest is None or zipinfo.file_size > largest.file_size:
            largest = zipinfo

    return largest


def _first_in_collection(col):
    """Return the first element from an ordered collection or an
    arbitrary element from an unordered collection. Raise StopIteration
    if the collection is empty.

    Source: https://stackoverflow.com/a/21067850
    """
    return next(iter(col))


def mkpath_if_not_exists(path):
    try:
        os.makedirs(path)
    except FileExistsError:
        if not os.path.isdir(path):
            raise NotADirectoryError(
                f'\'{path}\' already exists and is not a directory!'
            )


def list_issubset(child, parent):
    return set(child).issubset(set(parent))
