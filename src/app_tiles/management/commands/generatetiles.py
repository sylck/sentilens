from django.core.management import BaseCommand, CommandError

from ._generator import generate


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('xy', nargs='*', type=int)

    def handle(self, *args, **options):
        xy_args = options['xy']
        kwargs = {}

        if len(xy_args) == 2:
            xymin, xymax = xy_args

            kwargs['xmin'], kwargs['ymin'] = [xymin] * 2
            kwargs['xmax'], kwargs['ymax'] = [xymax] * 2
        elif len(xy_args) == 4:
            kwargs['xmin'], kwargs['ymin'], kwargs['xmax'], kwargs['ymax'] = xy_args
        elif not len(xy_args) == 0:
            arg_count = len(xy_args)
            raise CommandError(
                f'Invalid argument number. [xy] must be a list of two '
                f'(xymin, xymax) or four integers (xmin, ymin, xmax, ymax), '
                f'not {arg_count}.'
            )

        generate(self, **kwargs)
