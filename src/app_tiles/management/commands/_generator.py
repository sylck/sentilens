import copy
import datetime
import errno
import json
import os
import shutil

from sentinelsat import SentinelAPI

from django.conf import settings
from django.contrib.gis.gdal import GDALRaster

from raster.models import RasterTile
from raster.tiles.const import INTERMEDIATE_RASTER_FORMAT, WEB_MERCATOR_SRID
from raster.tiles.utils import tile_index_range

from shapely.affinity import scale
from shapely.geometry import Polygon

from ...models import Coordinate, TileSet
from ..commands import _utils as utils
from ..commands import _const as const


class Generator:
    def __init__(self, command_obj):
        self._command_obj = command_obj
        self._year = None
        self._api = None
        self._tileset = None
        self._rasterlayer = None

        self._xmin = None
        self._ymin = None
        self._xmax = None
        self._ymax = None

        self._tileids_wo_products = []
        self._tileids_wo_products_yearly = []
        self._tiles_wo_products = []
        self._tiles_wo_tileids = []

        basedir = getattr(settings, 'BASE_DIR')
        self._workdir = os.path.join(basedir, 'gentiles')
        self._safedir = os.path.join(self._workdir, 'safe')
        self._tcidir = os.path.join(self._workdir, 'tci')

        self._world_tiles = None

    def setup(self):
        self._api = SentinelAPI(
            os.getenv('SENTINEL_API_USERNAME'),
            os.getenv('SENTINEL_API_PASSWORD')
        )

        # load tile ids and their geometries
        world_file_path = os.path.join(
            os.path.dirname(__file__),
            'data/s2a.geojson'
        )
        with open(world_file_path, 'r') as world_file:
            world_gj = json.load(world_file)
            self._world_tiles = world_gj['features']

    def set_year(self, year):
        self._year = year

        self._tileset, _ = TileSet.objects.get_or_create(year=self._year)
        self._rasterlayer = self._tileset.rasterlayer

        self._tileids_wo_products_yearly = copy.deepcopy(
            self._tileids_wo_products
        )

    def set_tile_range(self, xmin, ymin, xmax, ymax):
        self._xmin = xmin
        self._ymin = ymin
        self._xmax = xmax
        self._ymax = ymax

    def run(self):
        self._print(f'Building tiles for year = {self._year}')

        if all([self._xmin, self._ymin, self._xmax, self._ymax]):
            range_x = range(self._xmin, self._xmax + 1)
            range_y = range(self._ymin, self._ymax + 1)
        else:
            tile_cnt = 2 ** const.MAX_ZOOM_LEVEL
            range_x = range(tile_cnt)
            range_y = range(tile_cnt)

        for x in range_x:
            for y in range_y:
                self._cleanup_and_check_disk_space()
                self._gen_tile(x, y)

        self._print('Building tile pyramid')
        self._build_tile_pyramid()

        self._print('Computing tile set bounds')
        self._compute_set_tileset_bounds()

    def _get_workdir(self):
        utils.mkpath_if_not_exists(self._workdir)
        return self._workdir

    def _get_safedir(self):
        utils.mkpath_if_not_exists(self._safedir)
        return self._safedir

    def _get_tcidir(self):
        utils.mkpath_if_not_exists(self._tcidir)
        return self._tcidir

    def _get_free_space(self):
        """Return free space in bytes for disk where the working
        directory is located. Create it if it does not exist.
        """
        workdir = self._get_workdir()
        _, _, free = shutil.disk_usage(workdir)
        return free

    def _clean_workdir(self):
        workdir = self._get_workdir()

        root, dirs, files = next(os.walk(workdir))
        for d in dirs:
            dir_abs = os.path.join(root, d)
            shutil.rmtree(dir_abs)
        for f in files:
            file_abs = os.path.join(root, f)
            os.unlink(file_abs)

    def _remove_tmpfiles(self):
        workdir = self._get_workdir()

        root, dirs, files = next(os.walk(workdir))
        tmpfiles = [f for f in files if f.startswith('tmp')]
        for tf in tmpfiles:
            tf_abs = os.path.join(root, tf)
            os.unlink(tf_abs)

    def _cleanup_and_check_disk_space(self):
        # remove potential leftover temp files from previous iterations
        # and `gentiles` runs
        self._remove_tmpfiles()

        # try freeing some space if running low on space
        if self._get_free_space() < const.MIN_FREE_SPACE_BYTES:
            self._clean_workdir()

        # if no space even after cleaning, raise an exception
        if self._get_free_space() < const.MIN_FREE_SPACE_BYTES:
            raise IOError(
                errno.ENOSPC,
                'Not enough disk space to proceed. Please free up some space '
                'and try again.'
            )

    def _print(self, msg):
        self._command_obj.stdout.write(msg)
        pass

    def _make_temp_tif(self):
        return utils.make_temp_tif(self._get_workdir())

    def _add_tci_to_raster(self, raster, tci_path, raster_path):
        tci_raster = GDALRaster(tci_path)

        # reproject to Web Mercator if necessary
        if tci_raster.srid != WEB_MERCATOR_SRID:
            trtmpfile = self._make_temp_tif()
            tci_raster = tci_raster.transform(
                WEB_MERCATOR_SRID,
                INTERMEDIATE_RASTER_FORMAT,
                trtmpfile.name,
                const.GDAL_RESAMPLE_ALGORITHM
            )

        # merge() in rasterio.merge, called in
        # make_mosaic_from_paths(), if not manually defined, reads
        # nodata value from the first raster in the list of datasets
        # provided as its first argument. Sentinel-2 TCI images do not
        # have nodata value defined, it's defined in their SAFE
        # container, so the for loop below does the job of manually
        # defining that value. this way it's ensured that rasterio
        # knows what the nodata value is and that rasters where no
        # merging occurs, i.e. just loading and copying to blank
        # tempfile, don't end up in the database without having a
        # defined nodata value.
        for band in tci_raster.bands:
            band.nodata_value = const.SENTINEL2_NODATA_VALUE

        if raster is None:
            # take reprojected TCI as starting point
            shutil.copyfile(tci_raster.name, raster_path)
        else:
            paths = [
                tci_raster.name,
                raster_path
            ]
            utils.make_mosaic_from_paths(paths, raster_path)

        return GDALRaster(raster_path)

    def _try_reuse_raster(self, raster):
        xmin, ymin, xmax, ymax = tile_index_range(
            raster.extent, const.MAX_ZOOM_LEVEL
        )

        workdir = self._get_workdir()

        try:
            utils.try_reuse_raster_multiproc(
                raster, workdir, self._rasterlayer, xmin, ymin, xmax, ymax
            )
        except MemoryError:
            self._print(
                'Not enough memory to reuse raster using multiprocessing, '
                'falling back to sequential approach...'
            )
            utils.try_reuse_raster_singleproc(
                raster, workdir, self._rasterlayer, xmin, ymin, xmax, ymax
            )

    def _build_tile_pyramid(self):
        workdir = self._get_workdir()

        try:
            utils.build_tile_pyramid_multiproc(workdir, self._rasterlayer)
        except MemoryError:
            self._print(
                'Not enough memory to build tile pyramid using '
                'multiprocessing, falling back to sequential approach...'
            )
            utils.build_tile_pyramid_singleproc(workdir, self._rasterlayer)

    def _compute_set_tileset_bounds(self):
        # flush previous bounds definition, if any
        self._tileset.bounds.clear()

        zoom_level = const.TILESET_BOUNDS_COMPUTATION_ZOOM_LEVEL

        # noinspection PyUnresolvedReferences
        rastertiles = RasterTile.objects.filter(
            rasterlayer=self._rasterlayer,
            tilez=zoom_level
        )

        extent = Polygon()

        for rastertile in rastertiles:
            rast = rastertile.rast
            x = rastertile.tilex
            y = rastertile.tiley

            # ignore tiles with nodata. polygonizing tiles with nodata
            # makes use of finer algorithms and results in more complex
            # geometry than simple tiles which don't have nodata. tile
            # set extent polygon complexity is already high with simple
            # tiles, so additional complexity might adversely affect
            # performance and is thus avoided.
            if utils.has_nodata(rast):
                continue

            points = utils.tile_to_polygon_points(zoom_level, x, y)
            tile_polygon = Polygon(points)

            # add tile_polygon to extent
            extent = extent.union(tile_polygon)

        # downscale extent's convex hull. convex hull of a Polygon
        # covers area the Polygon itself doesn't cover, i.e. it
        # represents area for which current TileSet probably* doesn't
        # have tiles, so let's zoom in a bit.
        #
        # * some tiles were dismissed for the sake of performance, so
        #   this area might be covered by those tiles, but unlikely
        extent_cvxhull_scaled = scale(
            extent.convex_hull,
            const.EXTENT_CVXH_SCALE_F_X, const.EXTENT_CVXH_SCALE_F_Y
        )

        echs_points = list(extent_cvxhull_scaled.boundary.coords)
        echs_points.append(echs_points[0])

        bounds = utils.get_maximal_rectangle(echs_points)

        for bound in bounds:
            longitude, latitude = bound

            # noinspection PyUnresolvedReferences
            coord = Coordinate.objects.create(
                latitude=latitude, longitude=longitude
            )

            self._tileset.bounds.add(coord)

    def _gen_tile(self, x, y):
        """Generate tile at MAX_ZOOM_LEVEL for given TMS (x, y)
        coordinates.

        Basic workflow is as follows:
            1. get tile IDs which cover the area of tile (x, y) at
               MAX_ZOOM_LEVEL
            2. download and mosaic products for retrieved tile IDs
               until all pixels for given tile are filled or tile IDs
               are depleted. in each iteration a new tile slice is
               generated to check if all pixels are filled.
            3. if tile slice has been generated:
               * add tile slice to database, regardless of its contents
                 (if at least one product was found, it is assumed that
                  it provided some pixel data)
               * attempt recycling the generated mosaic before leaving
        """
        self._print(
            f'Trying to generate tile x = {x}, y = {y}'
        )

        if utils.tile_already_generated(self._rasterlayer, const.MAX_ZOOM_LEVEL, x, y):
            self._print('Tile already generated')
            return

        if (x, y) in self._tiles_wo_products:
            # it is previously logged that there are no products which
            # satisfy at least one tileid requirement for this x-y.
            # that is, tileids for this x-y are defined in official KML
            # file, but Sentinel satellites have never (at least until
            # code execution) captured images of that area.
            self._print('Tile with known missing products, skipping')
            return

        if (x, y) in self._tiles_wo_tileids:
            self._print('Tile with known missing tileids, skipping')
            return

        tileids = utils.get_tileids_for_area(
            self._world_tiles,
            utils.tile_to_geojson(const.MAX_ZOOM_LEVEL, x, y)
        )

        if len(tileids) == 0:
            self._print('No matching tileids found for tile')
            self._tiles_wo_tileids.append((x, y))
            return

        if utils.list_issubset(tileids, self._tileids_wo_products):
            self._print(
                'Tile contains tileids with known missing products for all '
                'time, skipping'
            )
            self._tiles_wo_products.append((x, y))
            return

        if utils.list_issubset(tileids, self._tileids_wo_products_yearly):
            self._print(
                'Tile contains tileids with known missing products for '
                'current year, skipping'
            )
            return

        raster = None
        rtmpfile = self._make_temp_tif()
        tslice = None
        tstmpfile = self._make_temp_tif()

        while len(tileids) > 0:
            tileid = tileids.pop()

            if tileid in self._tileids_wo_products_yearly:
                self._print(
                    f'Tileid {tileid} with known missing products, skipping'
                )
                continue

            product = utils.get_product(self._api, self._year, tileid)

            if product is None:
                self._print(f'No products for tileid {tileid}')
                self._tileids_wo_products_yearly.append(tileid)

                if not utils.product_exists(self._api, tileid):
                    self._print(
                        f'Images of tileid {tileid} have not been captured yet'
                    )
                    self._tileids_wo_products.append(tileid)

                continue

            tci_path = utils.download_tci(
                self._api, self._get_tcidir(), self._get_safedir(), product
            )
            raster = self._add_tci_to_raster(raster, tci_path, rtmpfile.name)
            tslice = utils.make_tslice(
                const.MAX_ZOOM_LEVEL, x, y, raster, tstmpfile.name
            )

            if not utils.has_nodata(tslice):
                # area of interest populated, any tiles that may remain
                # in `tileids` are not necessary
                break

        if tslice is not None:
            # at least one tileid had a matching product for given year
            utils.add_tile_to_db(
                tslice, self._rasterlayer, const.MAX_ZOOM_LEVEL, x, y
            )

            self._print('Trying to reuse generated raster')
            self._try_reuse_raster(raster)
        else:
            self._print('Tile generation failed')


def generate(command_obj, **kwargs):
    year_start = const.SENTINEL2_YEAR_OP_START
    year_end = datetime.datetime.now().year - 1

    generator = Generator(command_obj)
    generator.setup()

    # try to fetch xmin, ymin, xmax, ymax
    xmin = kwargs.get('xmin')
    ymin = kwargs.get('ymin')
    xmax = kwargs.get('xmax')
    ymax = kwargs.get('ymax')

    if all([xmin, ymin, xmax, ymax]):
        generator.set_tile_range(xmin, ymin, xmax, ymax)

    for year in range(year_start, year_end + 1):
        generator.set_year(year)
        generator.run()
