from django.apps import AppConfig


class AppTilesConfig(AppConfig):
    name = 'app_tiles'
