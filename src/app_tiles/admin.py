from django.contrib import admin

from .models import Coordinate, TileSet


admin.site.register([Coordinate, TileSet])
