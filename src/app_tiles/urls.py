from django.urls import path, register_converter

from .converters import ImageExtensionConverter
from .views import TileView


app_name = 'app_tiles'

register_converter(ImageExtensionConverter, 'ext')

urlpatterns = [
    path('<int:layer>/<int:z>/<int:x>/<int:y>.<ext:frmt>', TileView.as_view(), name='tile')
]
