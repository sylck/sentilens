from raster.algebra.const import BAND_INDEX_SEPARATOR
from raster.views import AlgebraView


class TileView(AlgebraView):
    def get_ids(self):
        # rgb requests don't have `layer` kwarg, so popping instead of just
        # getting the value to prevent confusion in AlgebraView.get()
        layer_id = self.kwargs.pop('layer')

        # make retrieved tiles transparent
        get = self.request.GET.copy()
        get['alpha'] = 1
        self.request.GET = get

        ids = {}
        band_keys = ['r', 'g', 'b']

        for i, band_key in enumerate(band_keys):
            key = BAND_INDEX_SEPARATOR.join([band_key, str(i)])
            ids[key] = layer_id

        self._layer_ids = ids

        return self._layer_ids
