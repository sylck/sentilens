from rest_framework import generics

from .serializers import TileSetModelSerializer
from ..models import TileSet


class TileSetIndexAPIView(generics.ListAPIView):
    queryset = TileSet.objects.all()
    serializer_class = TileSetModelSerializer
