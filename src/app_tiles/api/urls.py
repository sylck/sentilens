from django.urls import path

from .views import TileSetIndexAPIView


app_name = 'tilesets'

urlpatterns = [
    path('', TileSetIndexAPIView.as_view(), name='index'),  # /api/tilesets/
]
