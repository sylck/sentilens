from rest_framework import serializers

from ..models import Coordinate, TileSet


class CoordinateModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coordinate
        fields = [
            'latitude',
            'longitude',
        ]


class TileSetModelSerializer(serializers.ModelSerializer):
    bounds = CoordinateModelSerializer(many=True)

    class Meta:
        model = TileSet
        fields = [
            'year',
            'rasterlayer_id',
            'bounds',
        ]
