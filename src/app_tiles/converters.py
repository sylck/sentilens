class ImageExtensionConverter:
    regex = 'png|jpg|tif'

    def to_python(self, value):
        return value

    def to_url(self, value):
        return value
