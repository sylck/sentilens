from django.db import models
from raster.models import RasterLayer


class Coordinate(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()


class TileSetQuerySet(models.QuerySet):
    def create(self, **kwargs):
        year = kwargs.get('year')

        tileset = super(TileSetQuerySet, self).create(year=year)
        rasterlayer, _ = RasterLayer.objects.get_or_create(name=str(year))
        tileset.rasterlayer = rasterlayer
        tileset.save()

        return tileset


class TileSet(models.Model):
    rasterlayer = models.ForeignKey(
        RasterLayer, null=True, on_delete=models.CASCADE
    )
    year = models.IntegerField(unique=True)
    bounds = models.ManyToManyField(Coordinate)

    objects = TileSetQuerySet.as_manager()

    def __str__(self):
        return str(self.year)

    class Meta:
        ordering = ('year', )
